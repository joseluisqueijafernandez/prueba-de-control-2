'use strict';

fetch('https://rickandmortyapi.com/api/character')
  .then((response) => response.json())
  .then(console.log);

fetch('https://rickandmortyapi.com/api/episode')
  .then((response) => response.json())
  .then(console.log);

fetch('https://rickandmortyapi.com/api/location')
  .then((response) => response.json())
  .then(console.log);
