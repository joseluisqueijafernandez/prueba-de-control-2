"use strict";

const url = "https://randomuser.me/api";

async function datos(url) {
  const response = await fetch(url);
  console.log(response);
  const dato = await response.json();
  const buscar = dato.results;

  console.log(buscar);

  const usuarios = buscar.map(function (usuario) {
    return usuario.email;
  });
  console.log(usuarios);
}

datos(url);
