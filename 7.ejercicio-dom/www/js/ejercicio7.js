"use strict";

const meterboton = document.querySelector("body");
meterboton.innerHTML = "<button>Boton</button> <header></header>";
console.log(meterboton);

const meterDivParaCuadrado = document.querySelector("header");
meterDivParaCuadrado.innerHTML = "<div></div> ";
console.log(meterDivParaCuadrado);

const cuadrado = document.querySelector("div");
cuadrado.style.height = " 100px";
cuadrado.style.width = "100px";
cuadrado.style.border = "2px solid black";
cuadrado.style.backgroundColor = "red";

console.log(cuadrado);
