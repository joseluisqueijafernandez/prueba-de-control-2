"use strict";
const intervalo = setInterval(() => {
  const fechaHora = new Date();

  const horas = fechaHora.getHours();
  const minutos = fechaHora.getMinutes();
  const segundos = fechaHora.getSeconds();
  const dia = fechaHora.getDay();

  console.log(
    `Segundos:${segundos} Minutos:${minutos} Horas:${horas} Día:${dia}`
  );
}, 5000);

setTimeout(() => {
  clearInterval(intervalo);
}, 25000);
